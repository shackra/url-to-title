# URL To Title #

Ingresas una URL, te devuelve el título de la página.

Así de simple.

## Características ##

* Funciona como una API (como, no igual a una), gracias a [Flask](http://flask.pocoo.org/).

## Cómo usar ##

Flask creará un servidor en [http://localhost:5000](http://localhost:5000).
Envía una solicitud via `POST` a esa dirección con los siguientes datos:

```
#!json
{
	"url": "la-url-es-sin-http-al-comienzo.com"
}
```

y el servicio te devuelve un texto plano con el título de la página web, o un
mensaje indicando o que no hay título o que hubo otro tipo de error.