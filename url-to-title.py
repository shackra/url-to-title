#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import requests
from BeautifulSoup import BeautifulSoup
import HTMLParser
from flask import Flask
from flask import request

reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def get_title():
    if request.method == 'POST':
        url = request.form.get("url", None)
        if url is None:
            return "ola k ase enviando solicitud vacía o k ase"
        else:
            try:
                pagina = requests.get("http://" + url, stream=True)
                if pagina.status_code >= 400:
                    titulo = ("Error HTTP {} al intentar acceder a"
                              " la URL.".format(pagina.status_code))
                else:
                    if(pagina.headers['content-type'].startswith("text/html")):
                        try:
                            titulo = "[ {} ]".format(
                                BeautifulSoup(pagina.content).title.string)
                        except Exception:
                            titulo = "Esa URL no tiene título, una pena."
                    else:
                        titulo = ("Esa no parecer ser una página web,"
                                  " pero gracias por el enlace.")
            except Exception:
                titulo = ("Ups, servicio no encontrado. O la URL está mal "
                          "escrita, o no está disponible por el momento.")
            else:
                parser = HTMLParser.HTMLParser()
                return parser.unescape(
                    ' '.join(str(titulo.replace("\n", "")).split()))
    else:
        return "ola k ase usando método incorrecto o k ase"

    return

if __name__ == "__main__":
    app.run()
